package cdz.pricecatch.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.regex.Matcher;

import cdz.pricecatch.R;
import cdz.pricecatch.core.PriceService;
import cdz.pricecatch.db.ProductTable;
import cdz.pricecatch.model.ProductModel;
import cdz.pricecatch.parse.ParsePriceTask;

public class HandleProductActivity extends AppCompatActivity {
    private TextView mTitleTv;
    private TextView mPriceTv;
    private ImageView mImageIv;
    private ProductBroadcastReceiver mProductReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTitleTv = (TextView) findViewById(R.id.title);
        mImageIv = (ImageView) findViewById(R.id.image);
        mPriceTv = (TextView) findViewById(R.id.price);

        mProductReceiver = new ProductBroadcastReceiver(this, mPriceTv, mTitleTv, mImageIv);

        Intent intent = getIntent();
        String action = intent.getAction();

        if (Intent.ACTION_SEND.equals(action)) {
            String url = intent.getStringExtra(Intent.EXTRA_TEXT);
            Log.d("priceLog", "[handle] url: " + url);

            Matcher m = Patterns.WEB_URL.matcher(url);
            while (m.find()) {
                url = m.group();
            }

            // Special replace for AliExpres if it get URL without price
            url = url.replace("www.", "ru.");

            PriceService.parseProduct(this, url);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(PriceService.ACTION_PPRODUCT_ADDED);
        registerReceiver(mProductReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mProductReceiver);

    }

    public static class ProductBroadcastReceiver extends BroadcastReceiver {
        private Context mContext;
        private TextView mPriceTv;
        private TextView mTitleTv;
        private ImageView mImageIv;

        public ProductBroadcastReceiver(Context context, TextView priceTv, TextView titleTv, ImageView image) {
            mContext = context;
            mPriceTv = priceTv;
            mTitleTv = titleTv;
            mImageIv = image;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("onReceiveAdded", intent.getAction());
            if (intent.getAction().equals(PriceService.ACTION_PPRODUCT_ADDED)) {
                ProductModel product = ProductTable.get(context, intent.getStringExtra(ProductModel.PRODUCT_ID));
                Log.d("onReceiveAdded", "update UI product: " + product.id);
                if (product.price != 0f) {
                    mPriceTv.setText(String.valueOf(product.price));
                }
                if (product.title != null) {
                    mTitleTv.setText(product.title);
                }
                if (!TextUtils.isEmpty(product.image)) {
                    Picasso.with(mContext).load(product.image).into(mImageIv);
                }
            }
        }
    }

}
