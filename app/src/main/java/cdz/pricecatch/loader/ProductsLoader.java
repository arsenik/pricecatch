package cdz.pricecatch.loader;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;

import cdz.pricecatch.db.ProductTable;
import cdz.pricecatch.model.ProductType;

/**
 * Created by Cdz on 26.11.2015.
 */
public class ProductsLoader extends CursorLoader {
    public ProductsLoader(Context context) {
        super(context);
    }

    @Override
    public Cursor loadInBackground() {
        return ProductTable.cursor(getContext(), ProductType.ALIEXPRESS);
    }
}
