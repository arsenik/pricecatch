package cdz.pricecatch.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

/**
 * Created by arsenik on 11/26/15.
 */
public abstract class Model {
    public abstract ContentValues getValues();
    public abstract Uri getTableUri();

    public Uri insert(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        return contentResolver.insert(getTableUri(), getValues());
    }
}
