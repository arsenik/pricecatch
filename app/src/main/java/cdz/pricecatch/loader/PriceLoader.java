package cdz.pricecatch.loader;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;

import cdz.pricecatch.db.PriceTable;
import cdz.pricecatch.db.ProductTable;
import cdz.pricecatch.model.ProductType;

/**
 * Created by Cdz on 26.11.2015.
 */
public class PriceLoader extends CursorLoader {
    private String mProductId;

    public PriceLoader(Context context, String productId) {
        super(context);
        mProductId = productId;
    }

    @Override
    public Cursor loadInBackground() {
        return PriceTable.cursor(getContext(), mProductId);
    }
}
