package cdz.pricecatch.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

/**
 * Created by arsenik on 11/26/15.
 */
public class PriceDbProvider extends ContentProvider {
    private DatabaseHelper mOpenHelper;
    private static UriMatcher sUriMatcher;
    private final static int URI_PRODUCT = 1;
    private final static int URI_PRICE = 2;
    public static final String AUTHORITY = "cdz.pricecatch";

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(AUTHORITY, ProductTable.TABLE_NAME, URI_PRODUCT);
        sUriMatcher.addURI(AUTHORITY, PriceTable.TABLE_NAME, URI_PRICE);
    }

    @Override
    public boolean onCreate() {
        final Context context = getContext();
        mOpenHelper = new DatabaseHelper(context);
        return true;
    }

    public int getMatchCode(Uri uri) {
        final int match = sUriMatcher.match(uri);
        return match;
    }

    private String getTableName(int match) {
        switch (match) {
            case URI_PRODUCT:
                return ProductTable.TABLE_NAME;
            case URI_PRICE:
                return PriceTable.TABLE_NAME;
        }
        return null;
    }

    private Uri getContentUri(int match) {
        switch (match) {
            case URI_PRODUCT:
                return ProductTable.CONTENT_URI;
            case URI_PRICE:
                return PriceTable.CONTENT_URI;
        }
        return null;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        final SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        int match = getMatchCode(uri);
        Cursor cursor = db.query(getTableName(match), projection, selection,
                selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(),
                getContentUri(match));
        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int match = getMatchCode(uri);
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        long rowID = db.insert(getTableName(match), null, values);
        Uri resultUri = ContentUris.withAppendedId(getContentUri(match), rowID);
        getContext().getContentResolver().notifyChange(resultUri, null);
        return resultUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        int match = getMatchCode(uri);
        int count = db.delete(getTableName(match), selection, selectionArgs);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
