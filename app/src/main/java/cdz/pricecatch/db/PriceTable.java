package cdz.pricecatch.db;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import cdz.pricecatch.model.PriceModel;

/**
 * Created by arsenik on 11/26/15.
 */
public class PriceTable {
    public static final Uri CONTENT_URI = Uri.parse("content://"
            + PriceDbProvider.AUTHORITY + "/" + PriceTable.TABLE_NAME);

    public static final String TABLE_NAME = "price";

    private static final String DROP_QUERY = "DROP TABLE IF EXISTS " + TABLE_NAME;
    private static final String CREATE_QUERY = "create table "
            + TABLE_NAME + " ("
            + BaseColumns._ID + " integer primary key autoincrement, "
            + PriceModel.PRODUCT_ID + " integer, "
            + PriceModel.SHOP + " integer, "
            + PriceModel.PRICE + " float default 0, "
            + PriceModel.TIME + " integer); ";

    public static final void create(SQLiteDatabase db) {
        db.execSQL(CREATE_QUERY);
    }

    public static final void drop(SQLiteDatabase db) {
        db.execSQL(DROP_QUERY);
    }

    public static Cursor cursor(Context context, String productId) {
        ContentResolver contentResolver = context.getContentResolver();
        StringBuilder where = new StringBuilder();
        where.append(PriceModel.PRODUCT_ID + " = " + productId);
        Log.d("priceCursorWhere", where.toString());
        Cursor cursor = contentResolver.query(PriceTable.CONTENT_URI, null,
                where.toString(), null, PriceModel.TIME + " DESC");
        return cursor;
    }

    public static List<PriceModel> list(Context context, int shop, String productId) {
        Cursor cursor = null;
        ContentResolver contentResolver = context.getContentResolver();
        List<PriceModel> list = new ArrayList<PriceModel>();

        try {
            StringBuilder where = new StringBuilder();
            where.append(PriceModel.PRODUCT_ID + " = " + productId);
            where.append(" AND " + PriceModel.SHOP + " = " + shop);

            cursor = contentResolver.query(PriceTable.CONTENT_URI, null,
                    where.toString(), null, PriceModel.TIME + " DESC");
            while (cursor != null && cursor.moveToNext()) {
                list.add(new PriceModel(cursor));
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return list;
    }
}
