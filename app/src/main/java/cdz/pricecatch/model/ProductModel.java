package cdz.pricecatch.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import cdz.pricecatch.db.ProductTable;

/**
 * Created by arsenik on 11/25/15.
 */
public class ProductModel extends Model {
    public static final String PRODUCT_ID = "productId";
    public static final String PRICE = "price";
    public static final String TITLE = "title";
    public static final String IMAGE = "image";
    public static final String URL = "url";
    public static final String SHOP = "shop";

    public String id;
    public String title;
    public String image;
    public String url;
    public float price;
    public int shop;

    public ProductModel(String id, String title, float price, String image, String url, int shop) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.image = image;
        this.url = url;
        this.shop = shop;
    }

    public ProductModel(Cursor cursor) {
        id = cursor.getString(cursor.getColumnIndex(PRODUCT_ID));
        title = cursor.getString(cursor.getColumnIndex(TITLE));
        image = cursor.getString(cursor.getColumnIndex(IMAGE));
        url = cursor.getString(cursor.getColumnIndex(URL));
        price = cursor.getFloat(cursor.getColumnIndex(PRICE));
        shop = cursor.getInt(cursor.getColumnIndex(SHOP));
    }

    public ContentValues getValues() {
        ContentValues cv = new ContentValues();
        cv.put(ProductModel.PRODUCT_ID, id);
        cv.put(ProductModel.TITLE, title);
        cv.put(ProductModel.PRICE, price);
        cv.put(ProductModel.IMAGE, image);
        cv.put(ProductModel.URL, url);
        cv.put(ProductModel.SHOP, shop);
        return cv;
    }

    @Override
    public Uri getTableUri() {
        return ProductTable.CONTENT_URI;
    }
}
