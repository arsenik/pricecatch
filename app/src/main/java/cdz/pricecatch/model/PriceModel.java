package cdz.pricecatch.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import cdz.pricecatch.db.PriceTable;

/**
 * Created by arsenik on 11/26/15.
 */
public class PriceModel extends Model {

    public static final String PRODUCT_ID = "productId";
    public static final String PRICE = "price";
    public static final String TIME = "time";
    public static final String SHOP = "shop";

    public String id;
    public float price;
    public long time;
    public int shop;

    public PriceModel(String id, float price, long time, int shop) {
        this.id = id;
        this.price = price;
        this.time = time;
        this.shop = shop;
    }

    public PriceModel(Cursor cursor) {
        id = cursor.getString(cursor.getColumnIndex(PRODUCT_ID));
        price = cursor.getFloat(cursor.getColumnIndex(PRICE));
        time = Long.valueOf(cursor.getLong(cursor.getColumnIndex(TIME)));
        shop = cursor.getInt(cursor.getColumnIndex(SHOP));
    }

    public ContentValues getValues() {
        ContentValues cv = new ContentValues();
        cv.put(PriceModel.PRODUCT_ID, id);
        cv.put(PriceModel.PRICE, price);
        cv.put(PriceModel.TIME, time);
        cv.put(PriceModel.SHOP, shop);
        return cv;
    }

    @Override
    public Uri getTableUri() {
        return PriceTable.CONTENT_URI;
    }
}
