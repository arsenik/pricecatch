package cdz.pricecatch.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import cdz.pricecatch.R;
import cdz.pricecatch.model.ProductModel;
import cdz.pricecatch.ui.PriceActivity;

/**
 * Created by Cdz on 26.11.2015.
 */
public class ProductsAdapter extends CursorAdapter {
    private LayoutInflater mInflater;

    public ProductsAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.product_row, null);
        ProductViewHolder holder = new ProductViewHolder();
        holder.title = (TextView) view.findViewById(R.id.title);
        holder.price = (TextView) view.findViewById(R.id.price);
        holder.image = (ImageView) view.findViewById(R.id.image);
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        ProductViewHolder holder = (ProductViewHolder) view.getTag();
        final ProductModel productModel = new ProductModel(cursor);
        Log.d("priceCursorWhere", "id: " + productModel.id);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PriceActivity.class);
                intent.putExtra(PriceActivity.PRODUCT_ID, productModel.id);
                Log.d("priceCursorWhere", "click id: " + productModel.id);
                context.startActivity(intent);
            }
        });

        holder.title.setText(productModel.title);
        holder.price.setText(String.valueOf(productModel.price));
        Picasso.with(context).load(productModel.image).into(holder.image);
    }
}
