package cdz.pricecatch.ui.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import cdz.pricecatch.model.ProductModel;

/**
 * Created by Cdz on 26.11.2015.
 */
public class ProductViewHolder {
    public TextView title;
    public TextView price;
    public ImageView image;
}
