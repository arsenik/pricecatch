package cdz.pricecatch.ui;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.widget.ListView;

import cdz.pricecatch.R;
import cdz.pricecatch.loader.ProductsLoader;
import cdz.pricecatch.ui.adapter.ProductsAdapter;

/**
 * Created by Cdz on 26.11.2015.
 */
public class ProductsActivity extends FragmentActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private ProductsAdapter mProductsAdapter;
    private ListView mListView;
    private final static int PRODUCT_LOADER_ID = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        mListView = (ListView) findViewById(R.id.list);
        mProductsAdapter = new ProductsAdapter(this, null, false);
        mListView.setAdapter(mProductsAdapter);
        getSupportLoaderManager().initLoader(PRODUCT_LOADER_ID, null, this);
        getSupportLoaderManager().getLoader(PRODUCT_LOADER_ID).forceLoad();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new ProductsLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mProductsAdapter.swapCursor(data);
        mProductsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }
}
