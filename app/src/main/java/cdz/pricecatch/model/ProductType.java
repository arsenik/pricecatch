package cdz.pricecatch.model;

public class ProductType {
    public static int ALIEXPRESS = 1;
    public static int ULMART_RU = 2;
    public static int JD_COM = 3;
    public static int YANDEX_MARKET = 4;
}