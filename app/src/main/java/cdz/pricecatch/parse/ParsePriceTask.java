package cdz.pricecatch.parse;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.net.URL;
import java.text.NumberFormat;
import java.text.ParseException;

import cdz.pricecatch.db.ProductTable;
import cdz.pricecatch.model.PriceModel;
import cdz.pricecatch.model.ProductModel;
import cdz.pricecatch.model.ProductType;

public class ParsePriceTask extends AsyncTask<String, Void, ProductModel> {
    private Context mContext;

    public ParsePriceTask(Context context) {
        mContext = context;
    }

    @Override
    protected ProductModel doInBackground(String... params) {
        return parseProduct(mContext, params[0]);
    }

    public static ProductModel parseProduct(Context context, String url) {
        float price = 0f;
        String id = null;
        String title = null;
        String image = null;
        int shop = 0;

        try {
            Document doc = Jsoup.parse(new URL(url), 30000);
            Element idElement = null;
            Element titleElement = null;
            Element imageElement = null;
            Element priceElement = null;

            if (url.contains("m.aliexpress.com")) {
                shop = ProductType.ALIEXPRESS;

                idElement = doc.select("input[name=productId]").first();
                if (idElement != null) {
                    id = idElement.attr("value");
                }
                priceElement = doc.select("span[itemprop=highPrice]").first();
                if (priceElement != null) {
                    price = Float.valueOf(priceElement.text());
                }
                imageElement = doc.select("img[itemprop=image]").first();
                if (imageElement != null) {
                    image = imageElement.attr("src");
                }
                titleElement = doc.select("p[itemprop=name]").first();
                if (titleElement != null) {
                    title = titleElement.text();
                }
            } else if (url.contains("aliexpress.com")) {
                shop = ProductType.ALIEXPRESS;

                idElement = doc.select("#hid-product-id").first();
                if (idElement != null) {
                    id = idElement.attr("value");
                }

                priceElement = doc.select("#sku-discount-price").first();
                if (priceElement == null) {
                    Log.d("priceLog", "price [2]");
                    priceElement = doc.select("#sku-price").first();
                    Log.d("priceLog", "priceElement == null" + (priceElement == null));
                }

                if (priceElement != null) {
                    Log.d("priceLog", "priceElement --- ");
                    Log.d("priceLog", "price element text: " + priceElement.text());
                    NumberFormat format = NumberFormat.getInstance();
                    Number number = format.parse(priceElement.text());
                    price = number.floatValue();
                }
                imageElement = doc.select("img[data-role=thumb]").first();
                if (imageElement != null) {
                    image = imageElement.attr("src");
                }
                titleElement = doc.select("h1[itemprop=name]").first();
                if (titleElement != null) {
                    title = titleElement.text();
                }
            }

            Log.d("priceLog", "parsed url: " + url);
            Log.d("priceLog", "id: " + id);
            Log.d("priceLog", "title: " + title);
            Log.d("priceLog", "image: " + image);
            Log.d("priceLog", "price: " + price);
            Log.d("priceLog", "shop: " + shop);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long time = System.currentTimeMillis();
        Log.d("priceLog", "time: " + time);

        ProductModel productModel = new ProductModel(id, title, price, image, url, shop);
        if (ProductTable.get(context, id) == null) {
            Uri productUri = productModel.insert(context);
            Log.d("priceLog", "product uri: " + productUri.toString());
        }

        PriceModel priceModel = new PriceModel(id, price, time, shop);
        priceModel.insert(context);

        return productModel;
    }
}