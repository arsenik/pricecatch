package cdz.pricecatch.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import cdz.pricecatch.R;
import cdz.pricecatch.model.PriceModel;

/**
 * Created by arsenik on 11/27/15.
 */
public class PriceAdapter extends CursorAdapter {
    private LayoutInflater mInflater;

    public PriceAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.price_row, null);
        PriceViewHolder holder = new PriceViewHolder();
        holder.date = (TextView) view.findViewById(R.id.date);
        holder.price = (TextView) view.findViewById(R.id.price);
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        PriceViewHolder holder = (PriceViewHolder) view.getTag();
        PriceModel priceModel = new PriceModel(cursor);

        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy hh:mm");

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(priceModel.time);
        holder.date.setText(formatter.format(calendar.getTime()));
        holder.price.setText(String.valueOf(priceModel.price));
    }
}
