package cdz.pricecatch.ui;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.widget.ListView;

import cdz.pricecatch.R;
import cdz.pricecatch.loader.PriceLoader;
import cdz.pricecatch.loader.ProductsLoader;
import cdz.pricecatch.ui.adapter.PriceAdapter;
import cdz.pricecatch.ui.adapter.ProductsAdapter;

/**
 * Created by Cdz on 26.11.2015.
 */
public class PriceActivity extends FragmentActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    public static final String PRODUCT_ID = "productId";
    private PriceAdapter mPriceAdapter;
    private ListView mListView;
    private final static int PRICE_LOADER_ID = 2;
    private String mProductId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price);

        mListView = (ListView) findViewById(R.id.list);
        mPriceAdapter = new PriceAdapter(this, null, false);
        mListView.setAdapter(mPriceAdapter);
        mProductId = getIntent().getExtras().getString(PRODUCT_ID);

        Bundle args = new Bundle();
        args.putString(PRODUCT_ID, mProductId);
        getSupportLoaderManager().initLoader(PRICE_LOADER_ID, args, this);
        getSupportLoaderManager().getLoader(PRICE_LOADER_ID).forceLoad();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new PriceLoader(this, args.getString(PRODUCT_ID));
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mPriceAdapter.swapCursor(data);
        mPriceAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
