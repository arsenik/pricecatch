package cdz.pricecatch.core;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import cdz.pricecatch.model.ProductModel;
import cdz.pricecatch.parse.ParsePriceTask;

/**
 * Created by Cdz on 28.11.2015.
 */
public class PriceService extends IntentService {
    public static final String ACTION_PRICES_UPDATE = "cdz.pricecatch.ACTION_PRICES_UPDATE";
    public static final String ACTION_PARSE_PRODUCT = "cdz.pricecatch.ACTION_PARSE_PRODUCT";
    public static final String ACTION_PPRODUCT_ADDED = "cdz.pricecatch.ACTION_PPRODUCT_ADDED";
    public static final String MESSENGER = "cdz.pricecatch.Messenger";

    public static final String URL = "url";

    public static final String PRICE_SERVICE = "PriceService";
    public static final String LOCK_WIFI_NAME = "PriceWakeLock";
    private WifiManager.WifiLock wifiLock;

    public PriceService() {
        super(PRICE_SERVICE);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        WifiManager wm = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        wifiLock = wm.createWifiLock(WifiManager.WIFI_MODE_FULL, LOCK_WIFI_NAME);
        wifiLock.setReferenceCounted(true);
    }

    @Override
    public void onStart(Intent intent, final int startId) {
        wifiLock.acquire();
        super.onStart(intent, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (wifiLock != null && wifiLock.isHeld()) {
            wifiLock.release();
        }

        WakefulBroadcastReceiver.completeWakefulIntent(intent);
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        String action = intent.getAction();
        if (action != null) {
            Log.d("onReceiveAdded", "service handle: " + action);
            if (action.equals(ACTION_PRICES_UPDATE)) {

            } else if (action.equals(ACTION_PARSE_PRODUCT)) {
                String url = intent.getExtras().getString(URL);
                Log.d("onReceiveAdded", "parse url: " + url);
                ProductModel productModel = ParsePriceTask.parseProduct(this, url);

                Log.d("onReceiveAdded", "parsed product: " + productModel.id);

                Intent productIntent = new Intent(ACTION_PPRODUCT_ADDED);
                productIntent.putExtra(ProductModel.PRODUCT_ID, productModel.id);
                sendBroadcast(productIntent);
                Log.d("onReceiveAdded", "send broadcast product: " + productModel.id);

                Messenger messenger = intent.getParcelableExtra(MESSENGER);
                if (messenger != null) {
                    try {
                        messenger.send(Message.obtain());
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public static void parseProduct(Context context, String url) {
        Intent intent = new Intent(context, PriceService.class);
        intent.setAction(ACTION_PARSE_PRODUCT);
        intent.putExtra(URL, url);
        context.startService(intent);
    }

    public static void pricesUpdate(Context context) {
        Intent intent = new Intent(context, PriceService.class);
        intent.setAction(ACTION_PRICES_UPDATE);
        context.startService(intent);
    }
}
