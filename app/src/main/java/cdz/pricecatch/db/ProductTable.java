package cdz.pricecatch.db;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import cdz.pricecatch.model.PriceModel;
import cdz.pricecatch.model.ProductModel;

public class ProductTable {
    public static final Uri CONTENT_URI = Uri.parse("content://"
            + PriceDbProvider.AUTHORITY + "/" + ProductTable.TABLE_NAME);

    public static final String TABLE_NAME = "product";

    private static final String DROP_QUERY = "DROP TABLE IF EXISTS " + TABLE_NAME;
    private static final String CREATE_QUERY = "create table "
            + TABLE_NAME + " ("
            + BaseColumns._ID + " integer primary key autoincrement, "
            + ProductModel.PRODUCT_ID + " integer, "
            + ProductModel.PRICE + " float default 0, "
            + ProductModel.TITLE + " text, "
            + ProductModel.SHOP + " integer, "
            + ProductModel.IMAGE + " text, "
            + ProductModel.URL + " text); ";

    public static final void create(SQLiteDatabase db) {
        db.execSQL(CREATE_QUERY);
    }

    public static final void drop(SQLiteDatabase db) {
        db.execSQL(DROP_QUERY);
    }

    public static List<ProductModel> list(Context context) {
        return list(context, 0);
    }

    public static Cursor cursor(Context context, int shop) {
        ContentResolver contentResolver = context.getContentResolver();

        StringBuilder where = new StringBuilder();
        if (shop > 0) {
            where.append(PriceModel.SHOP + " = " + shop);
        }
        Cursor cursor = contentResolver.query(ProductTable.CONTENT_URI, null,
                where.toString(), null, BaseColumns._ID + " DESC");
        return cursor;
    }

    public static ProductModel get(Context context, String productId) {
        Cursor cursor = null;
        ContentResolver contentResolver = context.getContentResolver();
        StringBuilder where = new StringBuilder();
        ProductModel productModel = null;
        if (!TextUtils.isEmpty(productId)) {
            where.append(PriceModel.PRODUCT_ID + " = " + productId);
        }

        try {
            cursor = contentResolver.query(ProductTable.CONTENT_URI, null,
                    where.toString(), null, BaseColumns._ID + " DESC");
            if (cursor != null && cursor.moveToFirst()) {
                productModel = new ProductModel(cursor);
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return productModel;
    }

    public static List<ProductModel> list(Context context, int shop) {
        Cursor cursor = null;
        ContentResolver contentResolver = context.getContentResolver();
        List<ProductModel> list = new ArrayList<ProductModel>();

        StringBuilder where = new StringBuilder();
        if (shop > 0) {
            where.append(PriceModel.SHOP + " = " + shop);
        }

        try {
            cursor = contentResolver.query(ProductTable.CONTENT_URI, null,
                    where.toString(), null, BaseColumns._ID + " DESC");
            while (cursor != null && cursor.moveToNext()) {
                list.add(new ProductModel(cursor));
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return list;
    }
}